#!/bin/bash

SERVER="10.20.30.40"
PORT="8765"

mkdir -p /tmp/lirc
mkdir -p $(pwd)/remotes
mkdir -p $(pwd)/unused

# Loop into all configuration stored in $(pwd)/remotes
for i in $(ls $(pwd)/remotes/); do
  # Copy configuration on LIRC server, restart LIRC server, then copy instruction on host
  scp $(pwd)/remotes/$i root@$SERVER:/etc/lircd.conf
  ssh root@$SERVER '/etc/init.d/lirc stop && /etc/init.d/lirc start'
  irsend -a $SERVER:$PORT LIST pioneer "" | head -n -1 | cut -d ' ' -f 2 > /tmp/lirc/$i

  # Test all instructions
  for f in $(cat /tmp/lirc/$i); do
    echo "$i command $f"
    irsend -a $SERVER:$PORT SEND_ONCE pioneer $f
    sleep 2s
  done

  # Ask user what to do. If Y, then break infinate loop and go to next instructions
  # If N (in fact any other letter), remove instructions and move current configuration to $(pwd)/unused
  while true
  do
    beep
    read -p "Do you want to keep configuration (Y/N)?" -n 1 -r
    if [[ $REPLY =~ ^[Y]$ ]]
    then
      break
    else
      rm /tmp/lirc/$i
      mv $(pwd)/remotes/$i $(pwd)/unused/
      break
    fi
  done

done
