# Remote instructions for Pioneer set
Instructions are missing on official [remotes database](http://lirc-remotes.sourceforge.net/remotes-table.html).
My setup is following :
* DC-Z83 ;
* F-Z93L ;
* PD-Z73T.

I wrote a small `bash` script testing all instructions written for Pioneer Devices and picked up good one for my setup.
All configurations found [here](http://lirc.sourceforge.net/remotes/pioneer/) are saved in the folder remotes.

**Warning**, this script doesn't automatically download the configurations and I renamed everywhere the variable name to set pioneer.
